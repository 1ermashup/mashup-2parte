var Analizar =(function(){
  var apiKey = "key=AIzaSyBcwyQOLIVnYFep7gIjlIXMhgMVZJAhZ1k";
  var urlSafeBrowsing = "https://safebrowsing.googleapis.com/v4/threatMatches:find?" + apiKey;
  var URLseguros = [];
  var arregloEntidades = [];
  var arregloPaginasAnalizads = [];
  var arregloEntidadesFinal = [];

  var metodoPrincipal = function(arreglo){
    
    _verificarURL(arreglo);
    pagespeed();
    return obtenerResultadoFinal();
  }

  var procesarDatos = function(recurso_descargado){
    URLseguros=[];
    if(recurso_descargado.matches !== undefined){
      for(var i = 0; i < arregloEntidades.length; i++){
        var seguro = true;
          for(var j = 0; j < recurso_descargado.matches.length; j++){
            if(arregloEntidades[i].url === recurso_descargado.matches[j].threat.url){
              seguro = false;
            }
          }
          if(seguro){
            URLseguros.push(arregloEntidades[i]);
          }
      }
    }else{
      URLseguros = arregloEntidades;
    }
  }

var _verificarURL =  function(arreglo_entrada_URL){
  arregloEntidades=[];
  for(var i = 0; i < arreglo_entrada_URL.length; i++){
    if(arreglo_entrada_URL[i].url !== undefined){
      arregloEntidades.push(arreglo_entrada_URL[i]);
    }
  }

  var arrayURLprocesar = [];
  for(var i = 0; i < arregloEntidades.length; i++){
    arrayURLprocesar.push({"url": arregloEntidades[i].url});
  }

  var datos = {
      "client": {
        "clientId":      "ITO",
        "clientVersion": "1.5.2"
      },
      "threatInfo": {
        "threatTypes":      ["UNWANTED_SOFTWARE", "MALWARE", "SOCIAL_ENGINEERING"],
        "platformTypes":    ["WINDOWS"],
        "threatEntryTypes": ["URL"],
        "threatEntries": arrayURLprocesar
      }
    };

    var objeto_procesar = {
      "url": urlSafeBrowsing,
      "datosProcesar": datos,
      "procesarDatos": procesarDatos
    }

    XHR.post(objeto_procesar);
}

  var pagespeed = function(){
    var arrayURLanalizar = [];
    arregloPaginasAnalizads=[];
    for(var i = 0; i < URLseguros.length; i++){
      arrayURLanalizar[i] = {"url": URLseguros[i].url};
    }

    var asincrono = true;
    for(var i = 0; i <arrayURLanalizar.length; i++){
      if(i === arrayURLanalizar.length-1){
        asincrono = false;
      }
      var objeto_procesar = {
        "url": "https://www.googleapis.com/pagespeedonline/v2/runPagespeed?url=" + arrayURLanalizar[i].url + "&strategy=desktop&"+apiKey,
        "procesarDatos": procesarDatosPageSpeed,
        "asincrono": asincrono
      }

      XHR.getPageSpeed(objeto_procesar);
    }
  }

  var procesarDatosPageSpeed = function(recurso_descargado){
    
    var objeto = {"url" : recurso_descargado.id + "", "speed": recurso_descargado.ruleGroups.SPEED.score, "nombre": recurso_descargado.title};
    arregloPaginasAnalizads.push(objeto);
  }

  var ordenar = function(arregloOrdernar){
    arregloOrdernar.sort(function(a, b){
      if(a.speed > b.speed){
        return 1;
      }
      if(a.speed < b.speed){
        return -1
      }
      return 0;
    });

    return arregloOrdernar;
  }

  var obtenerResultadoFinal = function(){
    arregloEntidadesFinal=[];
    var aux1 = ordenar(arregloPaginasAnalizads);
    var aux2 = [];
    for(var i = aux1.length-1; i >= 0 ; i--){
      aux2.push(aux1[i]);
    }

    if(aux2.length > 3){
      for(var i = 0; i < 3; i++){
        arregloEntidadesFinal.push(aux2[i]);
      }
    }else{
      arregloEntidadesFinal = aux2;
    }

    return arregloEntidadesFinal;
  }


  return {
    "metodoPrincipal": metodoPrincipal
  }
})();
