var XHR = (function (){
var _geti = function(_url, _en_caso_de_exito, _en_caso_de_error) {
        var _detectar_cambios_de_estado = function(_xhr, _en_caso_de_exito, _en_caso_de_error) {
            return function() {
                if (_xhr.readyState === 4) {
                    if (_xhr.status >= 200 && _xhr.status <= 299) {
                        _en_caso_de_exito(_xhr);
                    } else {
                        _en_caso_de_error(_xhr);
                    }
                }
            };
        };
        var xhr = new XMLHttpRequest();
        xhr.open("GET", _url /*+ "?r=" + Math.random()*/, /*true*/false);     
        xhr.onreadystatechange = _detectar_cambios_de_estado(xhr, _en_caso_de_exito, _en_caso_de_error);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.send();
      
    };

var _get_urls = function(_urls, _continuacion) {
        var _estado = {
            "exitos": [],
            "errores": []
        };
        
        var _exito = function(_estado, _urls, _continuacion) {
            return function(_xhr) {
                _estado.exitos.push(_xhr);
                if ((_estado.exitos.length + _estado.errores.length) === _urls.length) {
                    _continuacion(_estado);
                }
            };
        };
        
        var _error = function(_estado, _urls, _continuacion) {
            return function(_xhr) {
                _estado.errores.push(_xhr);
                if ((_estado.exitos.length + _estado.errores.length) === _urls.length) {
                    _continuacion(_estado);
                }
            };
        };
        
        var _acumular_exitos = _exito(_estado, _urls, _continuacion);
        var _acumular_errores = _error(_estado, _urls, _continuacion);
      

        for (var i = 0; i < _urls.length; i++) {
          var urlDif = _urls[i].search("kgsearch");
         XHR.gett(_urls[i], _acumular_exitos, _acumular_errores);
            if(i===1){
                XHR.get(_urls[i]);
            }     
        }
    };
//
var _get_urls_people = function(_urls, _continuacion) {
        var _estado = {
            "exitos": [],
            "errores": []
        };
        
        var _exito = function(_estado, _urls, _continuacion) {
            return function(_xhr) {
                _estado.exitos.push(_xhr);
                if ((_estado.exitos.length + _estado.errores.length) === _urls.length) {
                    _continuacion(_estado);
                }
            };
        };
        
        var _error = function(_estado, _urls, _continuacion) {
            return function(_xhr) {
                _estado.errores.push(_xhr);
                if ((_estado.exitos.length + _estado.errores.length) === _urls.length) {
                    _continuacion(_estado);
                }
            };
        };
        
        var _acumular_exitos = _exito(_estado, _urls, _continuacion);
        var _acumular_errores = _error(_estado, _urls, _continuacion);
      

        for (var i = 0; i < _urls.length; i++) {
          
         XHR.gett(_urls[i], _acumular_exitos, _acumular_errores);
            
        }
    };
    //
  var _get = function(url){
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
      if(xhr.readyState === 4 && xhr.status === 200){
        var myjson = JSON.parse(xhr.responseText);
        
        Procesar.procesar(myjson);
        
      }
    };
    xhr.open('GET', url,false);
    xhr.send();
  };

  var _cambios = function(xhr, objeto_entrada){
    return function(){
      if (xhr.readyState === 4) {
        if(xhr.status >= 200 && xhr.status <= 299){
          var r= JSON.parse(xhr.responseText);
          objeto_entrada.procesarDatos(r);
        }else{
          console.log("HUBO UN ERROR");
        }
      }
    }
  }

  var _post = function(objeto_entrada){
      var xhr = new XMLHttpRequest();
      var url = objeto_entrada.url;
      xhr.open("POST", url,false);
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.onreadystatechange = _cambios(xhr, objeto_entrada);
      var obj = JSON.stringify(objeto_entrada.datosProcesar);
      xhr.send(obj);
  }

  var _getPageSpeed = function(objeto_entrada){
    var xhr = new XMLHttpRequest();
    var url  = objeto_entrada.url;
    xhr.open("GET", url,false);
    xhr.onreadystatechange = _cambios(xhr, objeto_entrada);
    xhr.send();
  }

  return {
    'get': _get,
    "post": _post,
    "getPageSpeed": _getPageSpeed,
    "gett":_geti,
    "get_urls":_get_urls,
    "get_urls_people":_get_urls_people
  };

})();
